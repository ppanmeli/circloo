#include "circle.hpp"
#include "vector2.hpp"
#include <cmath>

Circle::Circle():
	r {0,0}, v {0, 0}, rad{0}
{
}

Circle::Circle(float x, float y, float rad):
	r {x,y}, v {0, 0}, rad{rad}
{

}

bool Circle::colisao_circle(const Circle &c)
{
	return rad + c.rad >= sqrt(pow(r.x - c.r.x, 2) + pow(r.y - c.r.y, 2));
}
