#ifndef CIRCLE_H
#define CIRCLE_H
#include "vector2.hpp"
class Circle
{
public:
	//centro do círculo está em r, posição
	Vector2 r;
	Vector2 v;
	float rad;
	Circle();
	Circle(float x, float y, float rad);
	bool colisao_circle(const Circle &c);
};

#endif
