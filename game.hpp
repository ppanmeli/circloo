#ifndef Game_hpp
#define Game_hpp

#include <SDL.h>
#include "circle.hpp"
#include <vector>
using namespace std;


enum Commands {
	LEFT = 0, RIGHT, UP, DOWN
};

//classe jogo
class Game {

public:
	//construtor -> para quando abrir o jogo
	Game(int width, int height);
	//desconstrutor -> para quando fechar o jogo
	~Game();

	//para inicializar o jogo
	void init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen); //o bool fullscren serve para definirmos se iremos querer que seja true or false 

	void handleEvents();
	void update();
	void render();
	void clean();

	//para mostrarmos ao main que o jogo est� rodando
	bool running() { return isRunning; };


	void drawLine(int x0, int y0, int x1, int y1);
	void drawLine(const Vector2 &i, const Vector2 &f);
	void cleanScreen();
	void drawCircle(int x, int y, int r);

	//tomada de decisão
	void decisionMaker();

	//se está dentro do círculo
	bool isInside(const Circle &inner, const Circle &outter);

	//desenha a "flexa" do movimento parabólico, para o círculo principal realizar movimento parabólico
	void drawVector(const Vector2 &i, const Vector2 &f);

	//parabola
	void parabola();

//quando incializar a classe jogo
private:
	int cnt = 0;
	bool isRunning;
	SDL_Window *window;
	SDL_Renderer *renderer;
	Circle c;
	bool b[4] = {false, false, false, false}; //inicia as posições em zero do movimento do círculo principal
	vector < Circle > circles;

};
#endif // !Game_hpp
