#ifndef VECTOR2_hpp
#define VECTOR2_hpp

#include <iostream>
#include <SDL.h>
#include <vector>
using namespace std;

//classe vetor
class Vector2{
public:
    float x, y;
	//construtor
	Vector2(float X = 0, float Y = 0);
    Vector2 & operator+=(const Vector2 &v);
    Vector2 operator+(const Vector2 &v) const;
    Vector2 & operator-=(const Vector2 &v);
    Vector2 operator-(const Vector2 &v) const;
    Vector2 & operator*=(const float &e);
    Vector2 operator*(const float &e) const;
    friend Vector2 operator*(const float &e, const Vector2 &v);
    double operator*(const Vector2 &v) const;
    float abs();
    Vector2 unit();
    Vector2 proj_V(const Vector2 &v);
    float angle();
    /*float dotVector(const Vector &vec);*/
    friend ostream & operator<<(ostream &o, const Vector2 &v);
};

#endif
