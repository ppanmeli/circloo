#include "vector2.hpp"
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

//construtor
Vector2::Vector2(float x, float y):
    x{x}, y{y}
{

}  

Vector2 & Vector2::operator+=(const Vector2 &v)
{
    //Vector2 tmp(*this); 
    x+=v.x;
    y+=v.y;
    return *this;
}

Vector2 Vector2::operator+(const Vector2 &v) const
{
	Vector2 tmp(*this);
	tmp+=v;
	return tmp;
}

Vector2 & Vector2::operator-=(const Vector2 &v)
{
    //Vector2 tmp(*this); 
    x-=v.x;
    y-=v.y;
    return *this;
}

Vector2 Vector2::operator-(const Vector2 &v) const
{
	Vector2 tmp(*this);
	tmp-=v;
	return tmp;
}

Vector2 & Vector2::operator*=(const float &e)
{
	x*=e;
    y*=e;
    return *this;
}

Vector2 Vector2::operator*(const float &e) const
{
	Vector2 tmp(*this);
	tmp*=e;
	return tmp;
}

Vector2 operator*(const float &e, const Vector2 &v)
{
	return (v*e);
}

double Vector2::operator*(const Vector2 &v) const
{
	return (x*v.x)+(y*v.y);
}

ostream & operator<<(ostream &o, const Vector2 &v)
{
   o << "(" << v.x << "," << v.y << ")";
   return o;
}



//calcula e retorna a magnitude (comprimento) do vetor
float Vector2::abs()
{
	return sqrtf(x * x + y * y);
}

//normaliza o vetor (assumi por ora que um vetor padrão possui o comprimento de 1)
Vector2 Vector2::unit()
{
	Vector2 tmp(*this);
	float u = abs();
	tmp.x /= u;
	tmp.y /= u;
	return tmp;
}

Vector2 Vector2::proj_V(const Vector2 &v)
{
	return((*this*v)/(v*v))*v;
}

float Vector2::angle()
{
	return atan2(y, x);
}

/*
//multiplica o vetor por um escalar (vetor com conservação de direção e sentido, porém com módulo alterado)
Vector operator*(float num) const
{
	return Vector(x * num, y * num);
}

//passa pelo vetor e pelo escalar, retornando seu produto
friend Vector operator*(float num, Vector const &vec)
{
	return Vector(vec.x * num, vec.y * num);
}

//adiciona dois vetores
Vector operator+(const Vector &vec) const
{
	return Vector(x + vec.x, y + vec.y);
}

//subtrai dois vetores
Vector operator-(const Vector &vec) const
{
	return Vector(x - vec.x, y - vec.y, z - vec.z);
}


	
//calcula e retorna o produto escalar (unção binária definida entre dois vetores que fornece um número real como resultado)
float dotVector(const Vector &vec) const
{
	return x * vec.x + y * vec.y + z * vec.z;
}
*/
