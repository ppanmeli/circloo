#include "circle.hpp"
#include "game.hpp"
#include "vector2.hpp"
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//construtor
Game::Game(int width, int height):
	c(width/2, height/2, 50)
{
	init("CircloO", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, false); //o bool false diz que o jogo não será rodado em fullscreen
}
//desconstrutor
Game::~Game()
{}

//inicializa o SDL
void Game::init(const char *title, int xpos, int ypos, int widht, int height, bool fullscreen)
{
	int flags = 0; //de acordo com o valor booleano inserido, o jogo inicializará ou não em tela cheia
	if (fullscreen)
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}

	
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0) 
	{
		std::cout << "Subsistema inicializado..." << std::endl; //Se o SDL inciar corretamente a mensagem aparecerá no console

		window = SDL_CreateWindow(title, xpos, ypos, widht, height, flags); //Criar tela *atenção a flags*
		if (window)
		{
			std::cout << "Tela criada..." << std::endl; //Se a tela for criada com sucesso, a mensagem aparecerá no console
		}

		renderer = SDL_CreateRenderer(window, -1, 0); //passa a window, index e flag
		if (renderer)
		{
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); //cor do renderizador
			std::cout << "Renderizador criado..." << std::endl;
		}

		
		isRunning = true; //se o SDL inicializar corretamente
	} else {
		isRunning = false; //se o SDL n�o inicializar corretamente
	}
	//carregar círculos
	std::ifstream entrada ("circulos.txt", std::ifstream::in);
	int linhas;
	entrada >> linhas;
	
	int x, y, r;
	char c;
	for (int l = 0; l < linhas; l++){
		entrada >> x >> c >> y >> c >> r;
		circles.push_back(Circle(x, y, r));
		//cout << x << "," << y << "," << r << endl;
	}
	entrada.close();
}

void Game::handleEvents()
{
	 //Quando realizar clique no X da janela, o jogo fechará
	SDL_Event event;
	//capturar eventos
	while(SDL_PollEvent(&event)){
		switch (event.type){
			case SDL_QUIT:
				isRunning = false;
			break;
			//para ativar os movimentos pelas setas do teclado, left e right
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
					case SDLK_LEFT:
						b[LEFT] = true;
					break;
					case SDLK_RIGHT:
						b[RIGHT] = true;
					break;
					case SDLK_UP:
						b[UP] = true;
					break;
					case SDLK_DOWN:
						b[DOWN] = true;
					break;
					default:
					break;
				}
			break;
			case SDL_KEYUP:
				switch (event.key.keysym.sym)
				{
					case SDLK_LEFT:
						b[LEFT] = false;
					break;
					case SDLK_RIGHT:
						b[RIGHT] = false;
					break;
					case SDLK_UP:
						b[UP] = false;
					break;
					case SDLK_DOWN:
						b[DOWN] = false;
					break;
					default:
					break;
				}
			break;
			//
			default:
			break;
		} //final do switch para identificação do evento
	} //final do processamento da fila de eventos

}

void Game::update()
{
	//Todos os objetos do jogo, toda a vez que os objetos forem implementados ele irá adicionar e dar update 
	cnt++;
	std::cout << cnt << std::endl;
}

void Game::render()
{
	cleanScreen();
	drawCircle(c.r.x, c.r.y, c.rad);
	/*SDL_RenderClear(renderer); //Limpa o renderizador*/
	//Aqui adiciona coisas para renderizar
	for (int c = 0; c < circles.size(); c++){
		drawCircle(circles[c].r.x, circles[c].r.y, circles[c].rad);
		//cout << circles[c].x << endl;
	}
	//drawLine(c.x, c.y, circles[0].x, circles[0].y);
	drawVector(c.r, c.r + c.v);


	Vector2 vn =(c.r-circles[0].r).unit();
	Vector2 vt(vn.y, -vn.x);
	Vector2 a = vn * (circles[0].rad);
	Vector2 i = (circles[0].r + a)+50*vt;
	Vector2 f = (circles[0].r + a)+(-50)*vt;
	drawLine(i, f);

	//atualizar tela
	SDL_RenderPresent(renderer);
	//se a UPKEY estiver pressionada no momento

	
}

void Game::clean()
{
	//Limpa o jogo ap�s fechar a janela
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
	std::cout << "Jogo limpo..." << std::endl; //mensagem para confirma��o que o jogo foi limpo, vista no console
}

void Game::cleanScreen()
{
	Uint8 r, g, b, a;
	SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

        /* Clear the entire screen to our selected color. */
    SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
}
void Game:: drawLine(int x0, int y0, int x1, int y1)
{
	SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
	SDL_RenderDrawLine (renderer, x0, y0, x1, y1);
	
}

void Game:: drawLine(const Vector2 &i, const Vector2 &f)
{
	SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
	SDL_RenderDrawLine (renderer, i.x, i.y, f.x, f.y);
}

void Game::drawCircle(int x, int y, int r)
{
	int lados = 60;
	double inc = 2*M_PI/lados;
	SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
	for (int l = 0;l<=lados; l++){
		SDL_RenderDrawLine (renderer, x+r*cos(l*inc),
			y+r*sin(l*inc), x+r*cos((l+1)*inc), y+r*sin((l+1)*inc));
		
	}

}

void Game::decisionMaker()
{
	//decisão
	double vMax = 1/2.0f;
	double gravG = 160;
	if(b[LEFT]){
		c.v.x+= -vMax;
		/*if (!isInside(c, circles[0]))
			c.v.x++;*/
	}
	if(b[RIGHT]){
		c.v.x+= +vMax;
		/*if (!isInside(c, circles[0]))
			c.v.x--;*/

	}	
	if(b[UP]){
		c.v.y+= -vMax;
		/*if (!isInside(c, circles[0]))
			c.v.y++;*/
	}
	if(b[DOWN]){
		c.v.y+= + vMax;
		/*if (!isInside(c, circles[0]))
			c.v.y--;*/
	}
	float dt = 1/100.0f;
	c.v = c.v+dt*Vector2(0, gravG);
	c.r = c.r + (dt)*c.v;
	if(!isInside(c, circles[0])){
		//criar um vetor com o tamanho máximo que o objeto pode estar afastado do centro
		Vector2 a = (c.r - circles[0].r).unit() * (circles[0].rad - c.rad);
		c.r = circles[0].r + a;
		Vector2 vn =(c.r-circles[0].r).unit();
		Vector2 vt(vn.y, -vn.x);
		Vector2 vProj = (c.v.proj_V(vt));
		c.v = vProj;
	}
	cout << c.v <<endl;
	//cout << "Is inside: " << isInside(c, circles[0]) << endl;
}

bool Game::isInside(const Circle &inner, const Circle &outter)
{
	double d = sqrt((inner.r.x - outter.r.x)*(inner.r.x - outter.r.x) +
		(inner.r.y - outter.r.y)*(inner.r.y - outter.r.y));
	return d + inner.rad <= outter.rad;
}

void Game::drawVector(const Vector2 &i, const Vector2 &f)
{
	SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
	SDL_RenderDrawLine (renderer, i.x, i.y, f.x, f.y);
	Vector2 v = f-i; //realizando o operador de atribuição
	float a = v.angle();
	cout << a << endl;
	float m = v.abs();
	cout << m << endl;
	float x = f.x + (1/10.0f)*m*cos(3*M_PI/4.0f+a);
	float y = f.y + (1/10.0f)*m*sin(3*M_PI/4.0f+a);
	SDL_RenderDrawLine (renderer, f.x, f.y, x, y);
	x = f.x + (1/10.0f)*m*cos(5*M_PI/4.0f+a);
	y = f.y + (1/10.0f)*m*sin(5*M_PI/4.0f+a);
	SDL_RenderDrawLine (renderer, f.x, f.y, x, y);

}

void Game::parabola()
{

}



