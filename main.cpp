#include "game.hpp"
#include "circle.hpp"
#include "vector2.hpp"
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//instância do jogo
Game *game = nullptr;

void testeVector()
{
	Vector2 v1(1,2);
	Vector2 v2(3,4);
	cout << v1 << endl;
	cout << v2 << endl;
	v1+=v2;
	cout << v1 << endl;
	cout << (v2+v2) << endl;
	v1*=10.0f;
	cout << v1 << endl;
	cout << (v2*10.0f) << endl;
	cout << (10.0f*v2) << endl;
	Vector2 v3(10, 10);
	cout << v3.abs() << endl;
	cout << v3.unit() << endl;
	cout << v3.unit().abs() << endl;
	Vector2 v4(3,2);
	Vector2 v5(4,5);
	cout << v4*v5 << endl;
	cout << v4.proj_V(v5) <<endl;
	cout << v4.proj_V(Vector2(1,0)) << endl;
	cout << v4.proj_V(Vector2(0,1)) << endl;
}

int main(int argc, char *argv[]) {
	/*testeVector();
	return 0;*/
	//limitar o número de frames
	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frameStart;
	//bool b[2] = {0, 0}; //inicia as posições em zero do movimento do círculo principal	
	int frameTime;

	//inicializa um novo jogo
	game = new Game(800, 600);
	

	//loop do jogo
	while (game->running()) { 
		//se o jogo estiver rodando 
		frameStart = SDL_GetTicks(); 
		//tratamento de eventos
		game->handleEvents();
		

		game->decisionMaker();
		
		game->render();
		frameTime = SDL_GetTicks() - frameStart; //isso dará em milisegundos quanto tempo dará para pegar os eventos, update nos objetos e renderizar tudo

		//checa se precisamos dar delay na execução desses frames, pegando o delay e tirando o tempo que leva para executá-lo
		//deixa a renderização dos frames mais suaves
		if (frameDelay>frameTime){ //se o frameDelay é maior que o frameTime
			SDL_Delay(frameDelay - frameTime);
		}
	}


	//limpar o jogo
	game->clean();

	return 0;
}
